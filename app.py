from __future__ import unicode_literals
import os
from flask import Flask, request, abort, render_template, url_for
from argparse import ArgumentParser
from linebot import LineBotApi, WebhookHandler
from linebot.exceptions import InvalidSignatureError
from linebot.models import MessageEvent, TextMessage, TextSendMessage
import configparser

app = Flask(__name__)
app.config["DEBUG"] = True
app.config["JSON_AS_ASCII"] = False

# config
config = configparser.ConfigParser()
config.read('config.ini')
line_bot_api = LineBotApi(config.get('line-bot', 'channel_access_token'))
handler = WebhookHandler(config.get('line-bot', 'channel_secret'))

rjun_id = 'U383275b1a62c47bfb71f0abc8ce18bb7'
jyun_id = 'U89feb26685135e4d253f02bf578d71df'


@app.route('/', methods=['GET'])
def index():
    print(url_for('index'))
    return render_template('test.html')

# 接收 LINE 的資訊


@app.route("/callback", methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']

    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    try:
        print(body, signature)
        handler.handle(body, signature)

    except InvalidSignatureError:
        abort(400)
    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def message_text(event):
    if event.source.user_id == "Udeadbeefdeadbeefdeadbeefdeadbeef":
        return 0
    with open('log.txt', 'a') as fd:
        fd.write(f'{str(event)}\n')
    
    print(f'message_id = {event.message.id}')
    client_name = line_bot_api.get_profile(
        event.source.user_id).display_name
    print(f'name = {client_name}')
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text=f'{client_name} : {event.message.text}')
    )
    if event.source.user_id == rjun_id:
        print(f'message_text = {event.message.text}')
        if event.message.text == 'q':
            try:
                line_bot_api.push_message(
                    jyun_id, TextSendMessage(text='小胖快回訊息'))
            except LineBotApiError as e:
                print(e)
                line_bot_api.reply_message(
                    event.reply_token,
                    TextSendMessage(text='error'+event.message.text)
                )
    if event.source.user_id != rjun_id:
        print(f'message_text = {event.message.text}')
        try:
            line_bot_api.push_message(
                rjun_id, TextSendMessage(text=f'{client_name} : {event.message.text}'))
        except LineBotApiError as e:
            print(e)


@handler.default()
def default(event):
    with open('log.txt', 'a') as fd:
        fd.write(f'{str(event)}\n')
    print(event)
    if event.message.type == 'image':
        print(f'image_id = {event.message.id}')


@app.after_request
def add_header(r):
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r


if __name__ == "__main__":
    arg_parser = ArgumentParser(
        usage='Usage: python ' + __file__ + ' [--port <port>] [--help]'
    )
    arg_parser.add_argument('-p', '--port', type=int,
                            default=6971, help='port')
    arg_parser.add_argument('-d', '--debug', default=False, help='debug')
    options = arg_parser.parse_args()
    app.run(debug=options.debug, port=options.port)
