FROM python:3.7.2-stretch

WORKDIR /app

ADD . /app

RUN pip install -r requirements.txt

CMD python app.py
#docker build -t line-bot .    
#docker run -m 2G --name line-bot line-bot:latest

